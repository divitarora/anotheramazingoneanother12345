package com.maf.AnotherAmazingOneAnother;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnotherAmazingOneAnother12345Application {

	public static void main(String[] args) {
		SpringApplication.run(AnotherAmazingOneAnother12345Application.class, args);
	}

}
